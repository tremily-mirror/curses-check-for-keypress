.PHONY : all check dist clean

all : dummy_py

dummy_py : setup.py curses_check_for_keypress.py
	python setup.py install --home=~
	echo "dummy for Makefile dependencies" > $@

check : all
	python curses_check_for_keypress.py

dist :
	python setup.py sdist
	scp dist/curses_check_for_keypress*tar.gz einstein:public_html/code/python/

clean :
	python setup.py clean
	rm -rf build dist curses_check_for_keypress.egg-info
	rm -f dummy_py *.pyc
